%% GRT Log File Reader
% This script ingests a DEMO.LOG file created by a Grand Rapids
% Technologies EFIS.  It parses data from the AHRS and EIS units, saves
% them to Matlab structures, makes some plots, and saves the data to a MAT
% file.  I've noted via comments where any of the parsing I've done applies
% to my airplane; this occurs for the EIS Aux data.
% 
%
% Written by Russell Carpenter, N418TX (Van's RV-7A)
% Version 1.3 (01 Feb 2014 6:11 PM)
%
% References (provided by Jeff DeFouw of GRT):
% 1. "DEMO file format.txt" - EFIS DEMO file format 1
% 2. "M46_6Aux_list.txt" - Model 4000/6000 Serial Data List
% 3. "Interface Control Document - AHRS - 09J.pdf" - Interface Control
%     Document: AHRS/Air Data Computer Model No. GRT-AA-0301 
%
% Tip: When the EFIS creates a lot of little separate files, do this:
%      cp DEMO-20200301-112440.LOG DEMO-20200301-112440+0.LOG
%      cat DEMO-20200301-112440+* >> DEMO-20200301-112440++.LOG
%      

clear
clc
[logfname,logfpath] = uigetfile({'*.LOG';'*.log'},'Select a GRT LOG file');
if ~logfname
    error('No file selected')
else
    fname = [logfpath,logfname];
end

%% Determine epoch from demo file name
[~,name] = fileparts(fname);
yr = str2double(name(6:9));
mo = str2double(name(10:11));
dy = str2double(name(12:13));
hr = str2double(name(15:16));
mi = str2double(name(17:18));
se = str2double(name(19:20));
epoch = datenum(yr,mo,dy,hr,mi,se);

%% Helper functions
twobyte = @(bytes) 256*bytes(1) + bytes(2); % For EIS 2-byte data
twosc = @(x,b) -x(1)*2^(b-1) + sum(x(2:end).*2.^(b-2:-1:0)); % 2's complement for AHRS data

%% Determine file contents
disp('Determining file contents...')
neis = 0;
nahrs = 0;
nahrs1 = 0;
nahrs2 = 0;
fid = fopen(fname,'r','ieee-be');
while ~feof(fid)
    source = fread(fid,1,'uint8');
    timestamp = fread(fid,1,'uint32'); % msec since boot
    length = fread(fid,1,'uint32'); % bytes
    if isempty(source)
        break
    end
    switch source
        case {1,13} % AHRS1 or AHRS2
            nahrs = nahrs + 1;
            fseek(fid,2,'cof'); % Skip 2 word header
            sentid = fread(fid,1,'uint8');
            switch sentid
                case 254 % Hex FE = Sentence 1
                    nahrs1 = nahrs1 + 1;
                    fseek(fid,length-3,'cof');
                case 253 % Hex FD = Sentence 2
                    nahrs2 = nahrs2 + 1;
                    fseek(fid,length-3,'cof');
                otherwise% skip to next packet
                    fseek(fid,length-3,'cof');
            end               
        case 2 % EIS
            neis = neis + 1;
            status = fseek(fid,length,'cof');
        otherwise % skip to next packet
            status = fseek(fid,length,'cof');
    end
end
disp('Packets Found:')
disp([' EIS: ', num2str(neis)])
disp([' AHRS: ', num2str(nahrs)])
disp([' AHRS Sentence 1: ', num2str(nahrs1)])
disp([' AHRS Sentence 2: ', num2str(nahrs2)])

%% Preallocate arrays
disp('Pre-allocating memory...')
ahrs1(1:nahrs1) = struct('timestamp',nan,'mode',nan,'roll',nan,...
    'pitch',nan,'yaw',nan,'altp',nan,'roc',nan,'kias',nan,'iasrate',nan,...
    'accroll',nan,'normacc',nan,'chksum',nan);
ahrs2(1:nahrs2) = struct('timestamp',nan,'oat',nan);
eis(1:neis) = struct('timestamp',nan,'tach',nan,'cht1',nan,'cht2',nan,...
    'cht3',nan,'cht4',nan,'cht5',nan,'cht6',nan,'egt1',nan,'egt2',nan,...
    'egt3',nan,'egt4',nan,'egt5',nan,'egt6',nan,'aspd',nan,'alt',nan,...
    'volt',nan,'fuelf',nan,'unit',nan,'carb',nan,'rocsgn',nan,'oat',nan,...
    'oilt',nan,'oilp',nan,'aux1',nan,'aux2',nan,'aux3',nan,'aux4',nan,...
    'aux5',nan,'aux6',nan,'cool',nan,'eti',nan,'qty',nan,'hrs',nan,'min',...
    nan,'sec',nan,'endhrs',nan,'endmin',nan,'baro',nan,'tach2',nan,...
    'spare',nan,'chksum',nan);

%% Now load data
disp('Loading data...')
frewind(fid)
i1 = 0;
i2 = 0;
ie = 0;
while ~feof(fid)
    source = fread(fid,1,'uint8');
    timestamp = fread(fid,1,'uint32'); % msec since boot
    length = fread(fid,1,'uint32'); % bytes
    if isempty(source)
        break
    end
    switch source
        case {1,13} % AHRS1 or AHRS2
            fseek(fid,2,'cof'); % Skip 2 word header
            sentid = fread(fid,1,'uint8');
            switch sentid
                case 254 % Hex FE = Sentence 1
                    i1 = i1 + 1;
                    ahrs1(i1).timestamp = timestamp/1e3;
                    ahrs1(i1).mode = fread(fid,8,'ubit1')';
                    ahrs1(i1).roll = twosc(fread(fid,16,'ubit1')',16)*180/32768;
                    ahrs1(i1).pitch = twosc(fread(fid,16,'ubit1')',16)*180/32768;
                    ahrs1(i1).yaw = twosc(fread(fid,16,'ubit1')',16)*180/32768;
                    ahrs1(i1).altp = fread(fid,1,'uint16')-5000;
                    ahrs1(i1).roc = twosc(fread(fid,16,'ubit1')',16);
                    ahrs1(i1).kias = twosc(fread(fid,16,'ubit1')',16)/10*3600/6076;
                    ahrs1(i1).iasrate = twosc(fread(fid,16,'ubit1')',16)/50;
                    ahrs1(i1).accroll = twosc(fread(fid,16,'ubit1')',16)*180/32768;
                    ahrs1(i1).normacc = twosc(fread(fid,16,'ubit1')',16)/1e3;
                    ahrs1(i1).checksum = fread(fid,1,'uint8');
                case 253 % Hex FD = Sentence 2
                    i2 = i2 + 1;
                    ahrs2(i2).timestamp = timestamp/1e3;
                    ahrs2(i2).oat = twosc(fread(fid,8,'ubit1')',8);
                    fseek(fid,length-4,'cof'); % skip rest of this packet
                    nahrs2 = nahrs2 + 1;
                otherwise% skip to next packet
                    fseek(fid,length-3,'cof');
            end               
        case 2 % EIS
            % Note that *most* EIS data is positive, so formatting for
            % two's complement is required.  But if you have configured the
            % AUX ports so that negative data are possible, then since
            % these bytes are unsigned, you need to apply the two's
            % complement as with the AHRS data.   NOTE THIS IS NOT
            % DESCRIBED IN M46_6Aux_list.txt!! The safest thing to do is
            % use the TWOSC helper for all this data, the way I did above
            % for the AHRS data.
            ie = ie + 1;
            fseek(fid,3,'cof'); % Skip 3 word header
            data = fread(fid,length-3,'uint8');
            eis(ie).timestamp = timestamp/1e3;
            eis(ie).tach = twobyte(data( 1: 2));
            eis(ie).cht1 = twobyte(data( 3: 4));
            eis(ie).cht2 = twobyte(data( 5: 6));
            eis(ie).cht3 = twobyte(data( 7: 8));
            eis(ie).cht4 = twobyte(data( 9:10));
            eis(ie).cht5 = twobyte(data(11:12));
            eis(ie).cht6 = twobyte(data(13:14));
            eis(ie).egt1 = twobyte(data(15:16));
            eis(ie).egt2 = twobyte(data(17:18));
            eis(ie).egt3 = twobyte(data(19:20));
            eis(ie).egt4 = twobyte(data(21:22));
            eis(ie).egt5 = twobyte(data(23:24));
            eis(ie).egt6 = twobyte(data(25:26));
            %eis(ie).aux5 = twobyte(data(27:28)); % Main Curr for N418TX
            %eis(ie).aux6 = twobyte(data(29:30)); % Aux Curr for N418TX
            eis(ie).aux5 = twosc(bitget(twobyte(data(27:28)),16:-1:1,'uint16'),16);
            eis(ie).aux6 = twosc(bitget(twobyte(data(29:30)),16:-1:1,'uint16'),16);
            eis(ie).aspd = twobyte(data(31:32));
            eis(ie).alt  = twobyte(data(33:34));
            eis(ie).volt = twobyte(data(35:36))/10;
            eis(ie).fuelf= twobyte(data(37:38))/10;
            eis(ie).unit = data(39);
            eis(ie).carb = data(40);
            eis(ie).rocsgn = data(41);
            eis(ie).oat = data(42)-50;
            eis(ie).oilt = twobyte(data(43:44));
            eis(ie).oilp = data(45);
            eis(ie).aux1 = twobyte(data(46:47)); % R Fuel for N418TX
            eis(ie).aux2 = twobyte(data(48:49)); % L Fuel for N481TX
            eis(ie).aux3 = twobyte(data(50:51))/10; % MAP for N418TX
            eis(ie).aux4 = twobyte(data(52:53));
            eis(ie).cool = twobyte(data(54:55));
            eis(ie).eti = twobyte(data(56:57))/10;
            eis(ie).qty = twobyte(data(58:59))/10;
            eis(ie).hrs = data(60);
            eis(ie).min = data(61);
            eis(ie).sec = data(62);
            eis(ie).endhrs = data(63);
            eis(ie).endmin = data(64);
            eis(ie).baro = twobyte(data(65:66))/100;
            eis(ie).tach2 = twobyte(data(67:68));
            eis(ie).spare = data(69);
            eis(ie).chksum = data(70);
            neis = neis + 1;
        otherwise % skip to next packet
            fseek(fid,length,'cof');
    end
end

%% Plots
t1 = epoch+[ahrs1.timestamp]/86400;
t2 = epoch+[ahrs2.timestamp]/86400;
te = epoch+[eis.timestamp]/86400;

figure(1)
set(1,'name','EIS')

subplot(6,1,1)
ax = plotyy(te,[eis.tach],te,[eis.aux3]);
grid on
set(ax(1),'ylim',[900 3100])
set(ax(2),'ylim',[9 31])
set(ax(1),'ytick',1000:500:3000)
set(ax(2),'ytick',10:5:30)
datetick(ax(1),'x','HH:MM:SS','keeplimits')
datetick(ax(2),'x','HH:MM:SS','keeplimits')
legend('Tach','MAP')
xlim = get(gca,'xlim');
title(fname)
set(get(ax(1),'ylabel'),'string','RPM')
set(get(ax(2),'ylabel'),'string','"Hg')

subplot(6,1,2)
plot(te,[[eis.egt1];[eis.egt2];[eis.egt3];[eis.egt4]])
grid on
set(gca,'ylim',[800 1600])
set(gca,'ytick',800:200:1600)
set(gca,'xlim',xlim)
datetick('x','HH:MM:SS','keeplimits')
legend('EGT1','EGT2','EGT3','EGT4')
ylabel('� F')

subplot(6,1,3)
plot(te,[[eis.cht1];[eis.cht2];[eis.cht3];[eis.cht4]])
grid on
set(gca,'xlim',xlim)
datetick('x','HH:MM:SS','keeplimits')
legend('CHT1','CHT2','CHT3','CHT4')
ylabel('� F')

subplot(6,1,4)
plot(te,[[eis.qty];[eis.aux1]+[eis.aux2]],te,[eis.fuelf]);
grid on
set(gca,'xlim',xlim)
datetick(gca,'x','HH:MM:SS','keeplimits')
legend('Total','R+L','Flow')
ylabel('Gal, GPH')

subplot(6,1,5)
ax = plotyy(te,[eis.oilt],te,[eis.oilp]);
grid on
datetick(ax(1),'x','HH:MM:SS','keeplimits')
datetick(ax(2),'x','HH:MM:SS','keeplimits')
legend('OilT','OilP')
set(get(ax(1),'ylabel'),'string','� F')
set(get(ax(2),'ylabel'),'string','PSI')

subplot(6,1,6)
ax = plotyy(te,[[eis.aux5];[eis.aux6]],te,[eis.volt]);
grid on
datetick(ax(1),'x','HH:MM:SS','keeplimits')
datetick(ax(2),'x','HH:MM:SS','keeplimits')
set(ax(1),'ylim',[-20 80])
set(ax(2),'ylim',[10 15])
set(ax(1),'ytick',[-20 0 20 40])
set(ax(2),'ytick',[12 13 14 15])
legh = legend('Main Cur','Aux Cur','Volt');
set(legh,'Location','SouthEast')
set(get(ax(1),'ylabel'),'string','Amps')
set(get(ax(2),'ylabel'),'string','Volts')
axes(ax(2))
grid on

%%
% AHRS sentence 1 records "high rate" data which is way faster than we need
% for plotting, so decimate it down using the index array d
d = 1:20:nahrs1;

figure(2)
set(2,'name','AHRS')

subplot(4,1,1)
ax = plotyy(t1(d),[ahrs1(d).altp],t1(d),[ahrs1(d).kias]);
grid on
datetick(ax(1),'x','HH:MM:SS','keeplimits')
datetick(ax(2),'x','HH:MM:SS','keeplimits')
yl = get(gca,'ylim');
%set(ax(1),'ylim',[min([ahrs1(d).altp]) yl(2)])
%set(ax(2),'ylim',[0 200])
%set(ax(2),'ytick',0:50:200)
legend('AltP','KIAS')
set(get(ax(1),'ylabel'),'string','feet')
set(get(ax(2),'ylabel'),'string','knots')
title(fname)

subplot(4,1,2)
ax = plotyy(t1(d),[ahrs1(d).roc],t1(d),[ahrs1(d).normacc]);
grid on
datetick(ax(1),'x','HH:MM:SS','keeplimits')
datetick(ax(2),'x','HH:MM:SS','keeplimits')
set(get(ax(1),'ylabel'),'string','fps')
set(get(ax(2),'ylabel'),'string','G')
set(ax(1),'ylim',[-2000 7000])
set(ax(2),'ylim',[-2.5 2])
legh = legend('RoC','Acc_N');
set(legh,'Location','SouthEast')
set(ax(1),'ytick',[-2000 -1000 0 1000 2000 3000])
set(ax(2),'ytick',[-.5 0 .5 1 1.5 2])
axes(ax(2))
grid on

subplot(4,1,3)
ax = plotyy(t1(d),[[ahrs1(d).roll];[ahrs1(d).pitch]],t1(d),[ahrs1(d).yaw]);
grid on
datetick(ax(1),'x','HH:MM:SS','keeplimits')
datetick(ax(2),'x','HH:MM:SS','keeplimits')
legend('Roll','Pitch','Yaw')
set(get(ax(1),'ylabel'),'string','deg')
set(get(ax(2),'ylabel'),'string','deg')

subplot(4,1,4)
ax = plotyy(t1(d),[ahrs1(d).iasrate],t1(d),[ahrs1(d).accroll]);
grid on
datetick(ax(1),'x','HH:MM:SS','keeplimits')
datetick(ax(2),'x','HH:MM:SS','keeplimits')
legh = legend('IAS Rate','Slip');
set(legh,'Location','SouthEast')
set(get(ax(1),'ylabel'),'string','fps^2')
set(get(ax(2),'ylabel'),'string','deg')
set(ax(1),'ylim',[-20 60])
set(ax(2),'ylim',[-30 10])
axes(ax(2))
grid on

%% Compute density altitude from pressure alt and OAT
% Ignores contribution from relative humidity
h_rho = @(altp,oatc) 1.24*altp + 118.8*oatc - 1782;
oatc = 5/9*([eis.oat]-32);
oatci = interp1q(te(:),oatc(:),t1(:))';
altd = h_rho([ahrs1.altp],oatci);

%% Create mat file
[matfname,savepath] = uiputfile('*.mat','Save to MAT file',name);
if ~matfname
    error('No file selected')
else
    save([savepath,matfname])
end

%% Save PDFs to same location as mat file
chdir(savepath)
for i = 1:2,
    figure(i)
    print('-fillpage',[name,'-',get(gcf,'name')],'-dpdf')
end